Name:       harbour-sextant

# >> macros
# << macros

Summary:    Org-Mode for Sailfish
Version:    0.1
Release:    1
Group:      Applications/Productivity
License:    GPLv3+
URL:        https://redmine.casenave.fr/projects/harbour-sextant
Source0:    %{name}-%{version}.tar.bz2
#Source100:  harbour-sextant.yaml
Requires:   sailfishsilica-qt5 >= 0.10.9
BuildRequires:  pkgconfig(sailfishapp) >= 1.0.2
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Multimedia)
BuildRequires:  pkgconfig(Qt5Network)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5Sql)
BuildRequires:  ecl
BuildRequires:  eql5
BuildRequires:  qt5-plugin-platform-minimal
BuildRequires:  desktop-file-utils

%description
This application aims to be a full featured org-mode implementation usable in
SailfishOS.

For now only the text editing features are implemented.

%if "%{?vendor}" == "chum"
PackageName: Sextant
Type: desktop-application
Categories:
- Utilities
- Office
- TextEditor
Custom:
  Repo: https://redmine.casenave.fr/projects/harbour-sextant
Icon: https://redmine.casenave.fr/projects/harbour-sextant/repository/48/revisions/master/raw/icons/172x172/harbour-sextant.png
%endif

%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
qmake harbour-sextant.pro
make %{?_smp_mflags}
# << build pre



# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
%qmake5_install
# << install pre

# >> install post
# << install post

desktop-file-install --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
   %{buildroot}%{_datadir}/applications/*.desktop

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%license COPYING
%{_bindir}
%{_libdir}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
# >> files
# << files

%changelog
* Sat Sep 17 2022 Renaud Casenave-Péré <renaud@casenave-pere.fr> - 0.1-1
- First release
