# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

CONFIG += debug

LISP_FILES = make.lisp \
    lisp/system-index.txt \
    lisp/local-projects/sextant/options/config.lisp \
    lisp/local-projects/sextant/options/options.lisp \
    lisp/local-projects/sextant/options/all.lisp \
    lisp/local-projects/sextant/org/nodes.lisp \
    lisp/local-projects/sextant/org/cursor.lisp \
    lisp/local-projects/sextant/org/parser.lisp \
    lisp/local-projects/sextant/org/printer.lisp \
    lisp/local-projects/sextant/org/all.lisp \
    lisp/local-projects/sextant/models/actions.lisp \
    lisp/local-projects/sextant/models/commands.lisp \
    lisp/local-projects/sextant/models/cursor.lisp \
    lisp/local-projects/sextant/models/files-model.lisp \
    lisp/local-projects/sextant/models/org-model.lisp \
    lisp/local-projects/sextant/models/utils.lisp \
    lisp/local-projects/sextant/models/all.lisp \
    lisp/local-projects/sextant/sextant.lisp \
    lisp/local-projects/sextant/dependencies.sexp \
    lisp/local-projects/sextant/sextant.asd

lisp.output = libsextant.a
lisp.commands = $$PWD/sextant-bootstrap -platform minimal -make
lisp.input = LISP_FILES
lisp.CONFIG = combine target_predeps

QMAKE_EXTRA_COMPILERS += lisp

# The name of your application
TARGET = harbour-sextant

QMAKE_CXXFLAGS += -std=c++2a -Wno-parentheses -Wno-unused-local-typedefs -Wno-array-bounds -Wno-maybe-uninitialized -Wno-restrict
CONFIG += sailfishapp
LIBS += -L. -lsextant-parser -lsextant -lecl -leql5
QT   += widgets qml multimedia network quick sql

SOURCES += src/harbour-sextant.cc

DISTFILES += qml/harbour-sextant.qml \
    qml/cover/CoverPage.qml \
    qml/components/ListTextField.qml \
    qml/components/ActionComboBox.qml \
    qml/components/OrgDelegate.qml \
    qml/components/OrgLine.qml \
    qml/components/OrgText.qml \
    qml/components/OrgEdit.qml \
    qml/components/OverwriteDialog.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    qml/pages/Files.qml \
    qml/pages/Org.qml \
    qml/pages/Settings.qml \
    qml/pages/SelectFileDialog.qml \
    rpm/harbour-sextant.changes.in \
    rpm/harbour-sextant.changes.run.in \
    rpm/harbour-sextant.spec \
#    rpm/harbour-sextant.yaml \
#    translations/*.ts \
    harbour-sextant.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
# CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
# TRANSLATIONS += translations/harbour-sextant-de.ts
