TEMPLATE = lib
CONFIG += debug
TARGET = sextant-parser
DESTDIR = $$PWD
OBJECTS_DIR = $$PWD/tmp/parser/
INCLUDEPATH += $$PWD/external/lexy/include
DEFINES += LEXY_HAS_UNICODE_DATABASE=1
LIBS += -lecl
QMAKE_CXXFLAGS += -std=c++2a -Wno-parentheses -Wno-unused-local-typedefs -Wno-array-bounds -Wno-maybe-uninitialized -Wno-restrict

target.path = $$[QT_INSTALL_LIBS]

INSTALLS = target

SOURCES += \
    src/parser/parser.cc \
    src/parser/ast.cc

HEADERS+= \
    src/parser/grammar.hh \
    src/parser/parser.hh \
    src/parser/ecl_helpers.hh \
    src/parser/ast.hh \
    external/lexy/include/lexy/input/string_input.hpp \
    external/lexy/include/lexy/input/base.hpp \
    external/lexy/include/lexy/input/range_input.hpp \
    external/lexy/include/lexy/input/buffer.hpp \
    external/lexy/include/lexy/input/lexeme_input.hpp \
    external/lexy/include/lexy/input/argv_input.hpp \
    external/lexy/include/lexy/input/file.hpp \
    external/lexy/include/lexy/_detail/detect.hpp \
    external/lexy/include/lexy/_detail/buffer_builder.hpp \
    external/lexy/include/lexy/_detail/assert.hpp \
    external/lexy/include/lexy/_detail/type_name.hpp \
    external/lexy/include/lexy/_detail/string_view.hpp \
    external/lexy/include/lexy/_detail/std.hpp \
    external/lexy/include/lexy/_detail/code_point.hpp \
    external/lexy/include/lexy/_detail/stateless_lambda.hpp \
    external/lexy/include/lexy/_detail/iterator.hpp \
    external/lexy/include/lexy/_detail/invoke.hpp \
    external/lexy/include/lexy/_detail/nttp_string.hpp \
    external/lexy/include/lexy/_detail/tuple.hpp \
    external/lexy/include/lexy/_detail/lazy_init.hpp \
    external/lexy/include/lexy/_detail/memory_resource.hpp \
    external/lexy/include/lexy/_detail/config.hpp \
    external/lexy/include/lexy/_detail/unicode_database.hpp \
    external/lexy/include/lexy/_detail/integer_sequence.hpp \
    external/lexy/include/lexy/parse_tree.hpp \
    external/lexy/include/lexy/code_point.hpp \
    external/lexy/include/lexy/dsl/context_flag.hpp \
    external/lexy/include/lexy/dsl/unicode.hpp \
    external/lexy/include/lexy/dsl/option.hpp \
    external/lexy/include/lexy/dsl/sign.hpp \
    external/lexy/include/lexy/dsl/base.hpp \
    external/lexy/include/lexy/dsl/position.hpp \
    external/lexy/include/lexy/dsl/sequence.hpp \
    external/lexy/include/lexy/dsl/loop.hpp \
    external/lexy/include/lexy/dsl/separator.hpp \
    external/lexy/include/lexy/dsl/context_counter.hpp \
    external/lexy/include/lexy/dsl/integer.hpp \
    external/lexy/include/lexy/dsl/brackets.hpp \
    external/lexy/include/lexy/dsl/parse_as.hpp \
    external/lexy/include/lexy/dsl/bits.hpp \
    external/lexy/include/lexy/dsl/whitespace.hpp \
    external/lexy/include/lexy/dsl/lookahead.hpp \
    external/lexy/include/lexy/dsl/case_folding.hpp \
    external/lexy/include/lexy/dsl/production.hpp \
    external/lexy/include/lexy/dsl/eof.hpp \
    external/lexy/include/lexy/dsl/code_point.hpp \
    external/lexy/include/lexy/dsl/return.hpp \
    external/lexy/include/lexy/dsl/delimited.hpp \
    external/lexy/include/lexy/dsl/capture.hpp \
    external/lexy/include/lexy/dsl/context_identifier.hpp \
    external/lexy/include/lexy/dsl/recover.hpp \
    external/lexy/include/lexy/dsl/any.hpp \
    external/lexy/include/lexy/dsl/operator.hpp \
    external/lexy/include/lexy/dsl/char_class.hpp \
    external/lexy/include/lexy/dsl/digit.hpp \
    external/lexy/include/lexy/dsl/list.hpp \
    external/lexy/include/lexy/dsl/follow.hpp \
    external/lexy/include/lexy/dsl/newline.hpp \
    external/lexy/include/lexy/dsl/bom.hpp \
    external/lexy/include/lexy/dsl/repeat.hpp \
    external/lexy/include/lexy/dsl/branch.hpp \
    external/lexy/include/lexy/dsl/terminator.hpp \
    external/lexy/include/lexy/dsl/literal.hpp \
    external/lexy/include/lexy/dsl/error.hpp \
    external/lexy/include/lexy/dsl/until.hpp \
    external/lexy/include/lexy/dsl/symbol.hpp \
    external/lexy/include/lexy/dsl/member.hpp \
    external/lexy/include/lexy/dsl/identifier.hpp \
    external/lexy/include/lexy/dsl/expression.hpp \
    external/lexy/include/lexy/dsl/peek.hpp \
    external/lexy/include/lexy/dsl/if.hpp \
    external/lexy/include/lexy/dsl/scan.hpp \
    external/lexy/include/lexy/dsl/byte.hpp \
    external/lexy/include/lexy/dsl/ascii.hpp \
    external/lexy/include/lexy/dsl/token.hpp \
    external/lexy/include/lexy/dsl/combination.hpp \
    external/lexy/include/lexy/dsl/punctuator.hpp \
    external/lexy/include/lexy/dsl/choice.hpp \
    external/lexy/include/lexy/dsl/times.hpp \
    external/lexy/include/lexy/grammar.hpp \
    external/lexy/include/lexy/lexeme.hpp \
    external/lexy/include/lexy/error.hpp \
    external/lexy/include/lexy/encoding.hpp \
    external/lexy/include/lexy/action/base.hpp \
    external/lexy/include/lexy/action/match.hpp \
    external/lexy/include/lexy/action/parse_as_tree.hpp \
    external/lexy/include/lexy/action/parse.hpp \
    external/lexy/include/lexy/action/scan.hpp \
    external/lexy/include/lexy/action/validate.hpp \
    external/lexy/include/lexy/action/trace.hpp \
    external/lexy/include/lexy/input_location.hpp \
    external/lexy/include/lexy/visualize.hpp \
    external/lexy/include/lexy/callback/adapter.hpp \
    external/lexy/include/lexy/callback/base.hpp \
    external/lexy/include/lexy/callback/object.hpp \
    external/lexy/include/lexy/callback/string.hpp \
    external/lexy/include/lexy/callback/integer.hpp \
    external/lexy/include/lexy/callback/container.hpp \
    external/lexy/include/lexy/callback/composition.hpp \
    external/lexy/include/lexy/callback/noop.hpp \
    external/lexy/include/lexy/callback/constant.hpp \
    external/lexy/include/lexy/callback/bit_cast.hpp \
    external/lexy/include/lexy/callback/forward.hpp \
    external/lexy/include/lexy/callback/bind.hpp \
    external/lexy/include/lexy/callback/fold.hpp \
    external/lexy/include/lexy/callback/aggregate.hpp \
    external/lexy/include/lexy/token.hpp \
    external/lexy/include/lexy/callback.hpp \
    external/lexy/include/lexy/dsl.hpp \
    external/lexy/include/lexy_ext/parse_tree_doctest.hpp \
    external/lexy/include/lexy_ext/parse_tree_algorithm.hpp \
    external/lexy/include/lexy_ext/compiler_explorer.hpp \
    external/lexy/include/lexy_ext/shell.hpp \
    external/lexy/include/lexy_ext/report_error.hpp
