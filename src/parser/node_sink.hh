#ifndef NODE_SINK_HH
#define NODE_SINK_HH

#include "ecl_helpers.hh"

#include <lexy/callback/base.hpp>

namespace lexy
{
    struct nullopt;
}

namespace sextant
{
    namespace parser
    {

        struct _node_sink
        {
            cl_object _head = ECL_NIL;
            cl_object _tail = ECL_NIL;

            using return_type = cl_object;

            cl_object operator()(cl_object&& obj)
            {
                if (_head == ECL_NIL)
                    _head = _tail = obj;
                else
                {
                    ast_funcall("APPEND-NODE", _tail, obj);
                    _tail = obj;
                }

                return _head;
            }

            cl_object&& finish() &&
            {
                return LEXY_MOV(_head);
            }
        };

        struct _append_node
        {
            using return_type = cl_object;

            constexpr cl_object operator()(cl_object&& obj) const
            {
                return LEXY_MOV(obj);
            }

            cl_object operator()(lexy::nullopt&&) const
            {
                return ECL_NIL;
            }

            template <typename... Args>
            constexpr cl_object operator()(Args&&... args) const
            {
                return ast_funcall("LINK-NODES", args...);
            }

            auto sink() const
            {
                return _node_sink{};
            }
        };

        constexpr auto append_node = _append_node {};

    }
}

#endif // NODE_SINK_HH
