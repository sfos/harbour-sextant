#ifndef ECL_HELPERS_HH
#define ECL_HELPERS_HH

#include <iostream>
#include <string>
#include <cstring>
#include <locale>
#include <codecvt>

#include <ecl/ecl.h>

namespace sextant
{
    namespace parser
    {

        inline std::u32string to_u32string (const std::string& arg)
        {
            std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter;
            std::u32string u32str = converter.from_bytes (arg.c_str ());

            return u32str;
        }

        inline cl_object to_cl_object(bool arg)
        {
            return arg ? ECL_T : ECL_NIL;
        }

        inline cl_object to_cl_object(char arg)
        {
            return cl_code_char(ecl_make_fixnum((int)arg));
        }

        inline cl_object to_cl_object(int arg)
        {
            return ecl_make_integer(arg);
        }

        inline cl_object to_cl_object(unsigned int arg)
        {
            return ecl_make_unsigned_integer(arg);
        }

        inline cl_object to_cl_object(std::size_t arg)
        {
            return ecl_make_unsigned_integer(arg);
        }

        inline cl_object to_cl_object(const char* arg)
        {
            size_t len = strlen(arg);
            cl_object l_s = ecl_alloc_simple_extended_string(len);
            ecl_character* l_p = l_s->string.self;
            for (size_t i = 0; i < len; ++i)
                l_p[i] = arg[i];
            return l_s;
        }

        inline cl_object to_cl_object(const std::string& arg)
        {
            return to_cl_object(arg.c_str());
        }

        inline cl_object to_cl_object(const std::u32string& arg)
        {
            size_t len = arg.size();
            cl_object l_s = ecl_alloc_simple_extended_string(len);
            ecl_character* l_p = l_s->string.self;
            for (size_t i = 0; i < len; ++i)
                l_p[i] = arg[i];
            return l_s;
        }

        inline cl_object collect_args()
        {
            return ECL_NIL;
        }

        template <class T>
        cl_object collect_args(T arg)
        {
            return CONS(to_cl_object(arg), ECL_NIL);
        }

        template <class T, class... Args>
        cl_object collect_args(T arg, Args... args)
        {
            return CONS(to_cl_object(arg), collect_args(args...));
        }

        inline int count_args()
        {
            return 0;
        }

        template <class T>
        int count_args(T)
        {
            return 1;
        }

        template <class T, class... Args>
        int count_args(T arg, Args... args)
        {
            return 1 + count_args(args...);
        }

        inline cl_object ast_funcall(const char* fun)
        {
            cl_object l_fun = cl_find_symbol(2, ecl_make_constant_base_string(fun, -1),
                                             ecl_find_package("ORG"), NULL);

            if (l_fun == ECL_NIL)
                std::cerr << "Couldn't find " << fun << " in package org" << std::endl;

            cl_object l_ret = ECL_NIL;
            const cl_env_ptr l_env = ecl_process_env();
            ECL_CATCH_ALL_BEGIN(l_env)
            {
                ECL_UNWIND_PROTECT_BEGIN(l_env)
                {
                    l_ret = cl_funcall(1, l_fun);
                }
                ECL_UNWIND_PROTECT_EXIT {}
                ECL_UNWIND_PROTECT_END;
            }
            ECL_CATCH_ALL_END;
            return l_ret;
        }


        template <class... Args>
        cl_object ast_funcall(const char* fun, cl_object arg1, Args... args)
        {
            cl_object l_fun = cl_find_symbol(2, ecl_make_constant_base_string(fun, -1),
                                             ecl_find_package("ORG"), NULL);

            if (l_fun == ECL_NIL)
                std::cerr << "Couldn't find " << fun << " in package org" << std::endl;

            int count = count_args(args...) + 2;
            cl_object l_ret = ECL_NIL;
            const cl_env_ptr l_env = ecl_process_env();
            ECL_CATCH_ALL_BEGIN(l_env)
            {
                ECL_UNWIND_PROTECT_BEGIN(l_env)
                {
                    l_ret = cl_funcall(count, l_fun, arg1, args...);
                }
                ECL_UNWIND_PROTECT_EXIT {}
                ECL_UNWIND_PROTECT_END;
            }
            ECL_CATCH_ALL_END;
            return l_ret;
        }

        template <class... Args>
        cl_object ast_funcall(const char* fun, Args... args)
        {
            cl_object l_fun = cl_find_symbol(2, ecl_make_constant_base_string(fun, -1),
                                              ecl_find_package("ORG"), NULL);
            if (l_fun == ECL_NIL)
                std::cerr << "Couldn't find " << fun << " in package org" << std::endl;

            cl_object l_args = collect_args(args...);

            cl_object l_ret = ECL_NIL;
            const cl_env_ptr l_env = ecl_process_env();
            ECL_CATCH_ALL_BEGIN(l_env)
            {
                ECL_UNWIND_PROTECT_BEGIN(l_env)
                {
                    l_ret = cl_apply(2, l_fun, l_args);
                }
                ECL_UNWIND_PROTECT_EXIT {}
                ECL_UNWIND_PROTECT_END;
            }
            ECL_CATCH_ALL_END;
            return l_ret;
        }

    } // namespace parser

} // namespace sextant

#endif // ECL_HELPERS_HH
