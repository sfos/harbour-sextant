#ifndef PARSER_HH
#define PARSER_HH

#include <ecl/ecl.h>

namespace sextant
{
    namespace parser
    {

        cl_object parse_document(cl_object l_str);
        void init_parser_lib();

    }
}

#endif // PARSER_HH
