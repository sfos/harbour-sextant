#ifndef GRAMMAR_HH
#define GRAMMAR_HH

#include "ast.hh"
#include "node_sink.hh"

#include <string>
#include <vector>

#include "lexy/callback.hpp"
#include "lexy/dsl.hpp"

namespace sextant
{
    namespace parser
    {
        struct text_line
        {
            std::string text;
            std::string eol;
        };

        namespace grammar
        {
            namespace dsl = lexy::dsl;

            struct eof
            {
                static constexpr auto rule = dsl::eof;
                static constexpr auto value = lexy::construct<std::string>;
            };

            struct eol
            {
                static constexpr auto rule = dsl::capture(dsl::newline) | dsl::p<eof>;
                static constexpr auto value = lexy::as_string<std::string>;
            };

            struct line
            {
                static constexpr auto rule = dsl::terminator (dsl::p<eol>).opt_list (dsl::capture (dsl::code_point));
                static constexpr auto value = lexy::as_string<std::string> >>
                    lexy::callback<text_line>([](lexy::nullopt&&, std::string&& eol) {
                            return text_line{"", eol};
                        },
                        [](std::string&& str, std::string&& eol) {
                            return text_line{str, eol};
                        });
            };

            struct headline_depth
            {
                static constexpr auto rule = dsl::list(dsl::capture(dsl::lit_c<'*'>)) + dsl::ascii::blank;
                static constexpr auto value = lexy::as_string<std::string>;
            };

            struct org_headline
            {
                static constexpr auto rule = dsl::p<headline_depth> + dsl::p<line>;
                static constexpr auto value = lexy::callback<cl_object>([](std::string&& depth, lexy::nullopt&&) {
                    return ast_funcall("MAKE-ORG-HEADLINE", depth.size() - 1, "", depth, "");
                },
                    [](std::string&& depth, text_line&& line) {
                        return ast_funcall ("MAKE-ORG-HEADLINE", depth.size() - 1, to_u32string(line.text), to_u32string(depth + " " + line.text), line.eol);
                });
            };

            struct org_line
            {
                static constexpr auto rule = dsl::p<line>;
                static constexpr auto value = lexy::callback<cl_object>([](text_line&& line) {
                        return ast_funcall("MAKE-ORG-LINE", to_u32string(line.text), line.eol);
                    });
            };

            struct org_node
            {
                static constexpr auto rule = [] {
                    auto headline = dsl::peek(dsl::p<headline_depth>) >> dsl::p<org_headline>;
                    return headline | dsl::else_ >> dsl::p<org_line>;
                }();

                static constexpr auto value = lexy::forward<cl_object>;
            };

            struct document
            {
                static constexpr auto rule = dsl::terminator(dsl::eof).opt_list(dsl::p<org_node>);
                static constexpr auto value = append_node >> lexy::callback<cl_object>([](lexy::nullopt&&) {
                    return ast_funcall("MAKE-ORG-DOCUMENT", ast_funcall("MAKE-ORG-LINE", "", ""));
                },
                    [](cl_object head) {
                        return ast_funcall("MAKE-ORG-DOCUMENT", head);
                    });
            };
        }
    }
}

#endif // GRAMMAR_HH
