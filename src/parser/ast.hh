#ifndef AST_HH
#define AST_HH

#include "ecl_helpers.hh"

namespace sextant
{
    namespace parser
    {

        enum AST_T
        {
            AST_VOID = 0,
            AST_RAW_LINE,
            AST_DOCUMENT,
            AST_MAX
        };

        static const char* type_to_ctor[AST_MAX];

        template <AST_T type>
        struct ast
        {
            template <class... Args>
            ast(Args... args)
            {
                self = ast_funcall(type_to_ctor[type], args...);
            }

            cl_object self;
        };

    } // namespace parser
} // namespace sextant


#endif // AST_HH
