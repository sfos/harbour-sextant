#include "parser.hh"

#include "grammar.hh"

#include <string>
#include <locale>
#include <codecvt>
#include <cassert>

#include "lexy/input/string_input.hpp"
#include "lexy/action/parse.hpp"
#include "lexy_ext/report_error.hpp"

namespace sextant
{
    namespace parser
    {

        cl_object parse_document(cl_object l_str)
        {
            assert(ECL_STRINGP(l_str));

            std::string str;
            if (ECL_BASE_STRING_P(l_str))
                str = std::string((char*)l_str->base_string.self, l_str->base_string.fillp);
            else
            {
                ecl_character* l_p = l_str->string.self;
                std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter;
                str = converter.to_bytes((char32_t*)l_p, (char32_t*)(l_p + l_str->string.dim));
            }

            auto input = lexy::string_input<lexy::utf8_encoding>(str);
            auto result = lexy::parse<grammar::document>(input, lexy_ext::report_error);

            if (!result)
                return ECL_NIL;

            if (result.has_value())
                return result.value();

            return ECL_NIL;
        }

        cl_object parse_node(cl_object l_str)
        {
            assert(ECL_STRINGP(l_str));

            std::string str;
            if (ECL_BASE_STRING_P(l_str))
                str = std::string((char*)l_str->base_string.self, l_str->base_string.fillp);
            else
            {
                ecl_character* l_p = l_str->string.self;
                std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter;
                str = converter.to_bytes((char32_t*)l_p, (char32_t*)(l_p + l_str->string.dim));
            }

            auto input = lexy::string_input<lexy::utf8_encoding>(str);
            auto result = lexy::parse<grammar::org_node>(input, lexy_ext::report_error);

            if (!result)
                return ECL_NIL;

            if (result.has_value())
                return result.value();

            return ECL_NIL;
        }

        void init_parser_lib()
        {
            cl_object lexy = ecl_make_constant_base_string("LEXY", 4);
            if (cl_find_package(lexy) == ECL_NIL)
                ecl_make_package(lexy, ECL_NIL, ECL_NIL, ECL_NIL);
            si_select_package(lexy);
            ecl_def_c_function(ecl_read_from_cstring("parse-document"), (cl_objectfn_fixed)parse_document, 1);
            ecl_def_c_function(ecl_read_from_cstring("parse-node"), (cl_objectfn_fixed)parse_node, 1);
        }

    }
}
