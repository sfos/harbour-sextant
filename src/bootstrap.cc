#include "parser/parser.hh"

#include <string>

#include <QApplication>
#include <QtCore>
#include <eql5/eql.h>

int main(int argc, char** argv)
{
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts); // for Qt WebEngine
    QApplication qapp(argc, argv);

    EQL::ini(argc, argv);

    QStringList args(QCoreApplication::arguments());

    EQL eql;
    eql.printVersion();

    sextant::parser::init_parser_lib();

    si_select_package(ecl_make_constant_base_string("COMMON-LISP-USER", 16));

    std::string code("(progn (load \"load.lisp\")");

    if (args.contains("-repl")) {
      code += "(funcall (intern (symbol-name 'start-slynk) :sextant)) ";
      code += "(si:top-level)";
    }
    else {
      if (args.contains("-tests"))
        code += "(load \"tests.lisp\")";
      if (args.contains("-make"))
        code += "(load \"make.lisp\")";
    }

    code += ")";

    CL_CATCH_ALL_BEGIN(ecl_process_env())
    {
        si_safe_eval(2, ecl_read_from_cstring(code.c_str()), ECL_NIL);
    }
    CL_CATCH_ALL_END;

    return 0;
}
