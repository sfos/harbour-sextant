(uiop:define-package :sextant/models/utils
  (:use :cl :eql)
  (:export #:define-roles
           #:*empty-variant*))
(in-package :sextant/models/utils)

(defmacro define-roles (start &rest roles)
  (let ((n start))
    `(progn
       ,@(mapcar (lambda (role) `(defconstant ,role ,(incf n))) roles))))

(defvar *empty-variant* (qnew "QVariant"))
