(uiop:define-package :sextant/models/files-model
  (:use :cl :eql :options :alexandria
   :sextant/models/utils)
  (:export #:load-recentf
           #:save-recentf
           #:push-to-recent-files
           #:remove-from-files-list
           #:refresh-agenda-files-model
           #:make-agenda-files-model))
(in-package :sextant/models/files-model)

(define-roles #.|Qt.UserRole|
  +filename-role+
  +directory-role+
  +section-role+)

(defvar *recent-files-list* nil)

(defvar *agenda-files-list* nil)
(defvar *agenda-files-model* nil)
(defvar *empty-model-index* (qnew "QModelIndex"))

(defun limit-list-size (lst size)
  (if (= size 0)
      nil
      (let* ((l lst)
             (last (dotimes (i (1- size) l)
                     (setf l (cdr l)))))
        (when last
          (rplacd last nil))
        lst)))

(defun recentf-pathname (filename)
  (merge-pathnames (concatenate 'string "harbour-sextant/" filename)
                   (uiop:xdg-cache-home)))

(defun load-recentf (filename)
  (let ((recentf-pathname (recentf-pathname filename)))
    (when (probe-file recentf-pathname)
      (setf *recent-files-list* (limit-list-size (uiop:safe-read-file-form recentf-pathname
                                                           :package :sextant/models/files-model)
                                                 recent-files-size)))))

(defun save-recentf (filename)
  (let ((recentf-pathname (recentf-pathname filename)))
    (with-open-file (stream (ensure-directories-exist recentf-pathname)
                            :direction :output :if-exists :supersede)
      (prin1 *recent-files-list* stream))))

(defun push-to-recent-files (pathname)
  (when (notany (lambda (files-list)
                  (and (uiop:subpathp pathname (truename (parse-namestring (car files-list))))
                     (string= (pathname-type pathname) "org")))
                (cdr *agenda-files-list*))
    (setf *recent-files-list*
          (limit-list-size (remove-duplicates (push pathname *recent-files-list*)
                                              :from-end t
                                              :test #'uiop:pathname-equal)
                           recent-files-size))))

(defun remove-from-files-list (filepath)
  (let ((pathname (parse-namestring filepath)))
    (multiple-value-bind (sublist index)
        (loop with row = 0
              for list in *agenda-files-list*
              do (progn
                   (let ((index (position pathname (cdr list) :test #'uiop:pathname-equal)))
                     (if index
                         (return (values list (+ row index)))
                         (incf row (length (cdr list)))))))
      (when (and (not (null sublist)) (listp sublist))
        (|beginRemoveRows| *agenda-files-model* *empty-model-index* index index)
        (when (string= (car sublist) "recentf")
          (setf *recent-files-list* (remove pathname *recent-files-list* :test #'uiop:pathname-equal)))
        (rplacd sublist (remove pathname (cdr sublist) :test #'uiop:pathname-equal))
        (|endRemoveRows| *agenda-files-model*)))))

(defun collect-org-files (directory)
  (mapcar (lambda (dir)
            (cons dir
                  (sort (remove-if #'uiop:hidden-pathname-p (uiop:directory-files dir "*.org"))
                        #'string< :key #'pathname-name)))
          (flatten (labels ((collect-dirs (dir)
                              (let ((subdirs (remove-if
                                              (lambda (d)
                                                (uiop:hidden-pathname-p
                                                 (merge-pathnames
                                                  (car (last (pathname-directory d)))
                                                  (uiop:pathname-parent-directory-pathname d))))
                                              (uiop:subdirectories dir))))
                                (cons (concatenate 'string directory
                                                   (enough-namestring dir (truename directory)))
                                      (mapcar #'collect-dirs subdirs)))))
                     (collect-dirs (truename directory))))))

(defun refresh-agenda-files-model (agenda-files)
  (unless (null *agenda-files-model*)
    (|beginResetModel| *agenda-files-model*)
    (setf *recent-files-list* (limit-list-size *recent-files-list* recent-files-size))
    (setf *agenda-files-list*
          (cons (cons "recentf"
                      *recent-files-list*)
                (loop for dir-or-file in agenda-files
                      append (if (uiop:directory-exists-p dir-or-file)
                                 (collect-org-files dir-or-file)
                                 (list (directory-namestring dir-or-file)
                                       dir-or-file)))))
    (|endResetModel| *agenda-files-model*)))

(defun construct-agenda-files-model ()
  (let ((model (qnew "QAbstractListModel")))
      (qoverride model "rowCount(QModelIndex)"
                 (lambda (index)
                   (declare (ignore index))
                   (let ((len 0))
                     (mapc (lambda (list)
                             (incf len (length (cdr list))))
                           *agenda-files-list*)
                     len)))
      (qoverride model "data(QModelIndex,int)"
                 (lambda (index role)
                   (let* ((row (|row| index))
                          section
                          (item (when (> row -1)
                                  (loop for list = *agenda-files-list* then (cdr list)
                                        while list
                                        do (progn
                                             (setf section (caar list))
                                             (if (>= row (length (cdar list)))
                                                 (decf row (length (cdar list)))
                                                 (return (nth row (cdar list)))))))))
                     (if item
                         (case role
                           (#.+filename-role+
                            (qvariant-from-value
                             (enough-namestring item (directory-namestring item))
                             "QString"))
                           (#.+directory-role+
                            (qvariant-from-value
                             (if (string= section "recentf")
                                 (let* ((rel-namestring (enough-namestring (uiop:pathname-directory-pathname item) (truename (parse-namestring "~/"))))
                                        (filename (if (string= rel-namestring (directory-namestring item))
                                                      rel-namestring
                                                      (concatenate 'string "~/" rel-namestring))))
                                   filename)
                                 (directory-namestring item))
                             "QString"))
                           (#.+section-role+
                            (qvariant-from-value section "QString")))
                         *empty-variant*))))
      (qoverride model "roleNames()"
                 (lambda ()
                   (list (cons +filename-role+ "filename")
                         (cons +directory-role+ "directory")
                         (cons +section-role+ "section"))))
      (when *agenda-files-model*
        (qdelete *agenda-files-model*))
      (setf *agenda-files-model* model)
      (|setContextProperty| (|rootContext| qml:*quick-view*) "agendaFilesModel" *agenda-files-model*)))

(defun make-agenda-files-model (agenda-files &optional force)
  (when (or (not *agenda-files-model*) force)
    (construct-agenda-files-model))
  (refresh-agenda-files-model agenda-files))
