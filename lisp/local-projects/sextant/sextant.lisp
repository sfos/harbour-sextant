(uiop:define-package :sextant
  (:use :cl :eql :org :options ;; :models
        )
  (:export #:start-slynk
           #:stop-slynk
           #:slynkp
           #:refresh-agenda-files
           #:open-file
           #:close-file
           #:make-document-from-string
           #:open-from-string
           #:delete-file*
           #:start
           #:reload-qml
           #:set-qml))
(in-package :sextant)

(qrequire :quick)

(defun sym (symbol package)
  (intern (symbol-name symbol) package))

(let (slynkp)
  (defun start-slynk ()
    (unless (find-package :slynk)
      (require :ecl-quicklisp)
      (funcall (sym 'quickload :ql) :slynk))
    (funcall (sym 'create-server :slynk)
             :interface "0.0.0.0" :port 4005 :dont-close t :style :spawn)
    (setf slynkp t))

  (defun stop-slynk ()
    (when (find-package :slynk)
      (funcall (sym 'stop-server :slynk) 4005)
      (setf slynkp nil)))

  (defun slynkp () slynkp))

(defun setup ()
  (load-config-file "config.lisp")
  (load-recentf "recentf")
  (make-agenda-files-model agenda-files))

(defun cleanup ()
  (close-file)
  (save-recentf "recentf")
  (save-config-file "config.lisp"))

(defun refresh-agenda-files ()
  (refresh-agenda-files-model agenda-files))

(let (currently-open-file)
  (defun open-file (filepath)
    (let* ((pathname (parse-namestring filepath))
           (org-document (if (probe-file pathname)
                             (parse-document pathname)
                             (make-org-document (make-org-line "" "")))))
      (make-org-model pathname org-document)
      (initialize-commands pathname)
      (push-to-recent-files pathname)
      (setf currently-open-file pathname)
      nil))

  (defun close-file (&optional filepath)
    (let ((pathname (if filepath (parse-namestring filepath) currently-open-file)))
      (when pathname
        (save-commands pathname)
        (setf currently-open-file nil))))

  (defun make-document-from-string (str)
    (if (> (length str) 0)
        (parse-document str)
        (make-org-document (make-org-line "" ""))))

  (defun open-from-string (str)
    (let ((org-document (if (> (length str) 0)
                            (parse-document str)
                            (make-org-document (make-org-line "" "")))))
      (make-org-model nil org-document)
      (initialize-commands nil)
      nil)))

(defun delete-file* (filepath)
  (let ((pathname (parse-namestring filepath)))
    (handler-case (delete-file pathname)
      (file-error () (return-from delete-file* nil)))
    (remove-from-files-list filepath)
    t))

(defun terminate ()
  (cleanup)
  (qrun #'qquit))

(defun start ()
  (setup)
  (when slynk-at-startup-p
    (start-slynk))
  (ext:catch-signal ext:+SIGTERM+ :catch)
  (ext:set-signal-handler ext:+SIGTERM+ #'terminate)
  (qconnect qml:*quick-view* "statusChanged(QQuickView::Status)"
            (lambda (status)
              (case status
                (#.|QQuickView.Ready|
                   (qml-reloaded)))))
  (qconnect qml:*quick-view* "closing(QQuickCloseEvent*)"
            (lambda (close)
              (declare (ignore close))
              (cleanup))))

(defun reload-qml (&optional (url "http://localhost:8000/"))
  "Reload QML file from an url, directly on the device."
  (qrun*
    (let ((src (|toString| (|source| qml:*quick-view*))))
      (if (x:starts-with qml:*root* src)
          (|setSource| qml:*quick-view* (qnew "QUrl(QString)" (x:string-substitute url qml:*root* src)))
          (qml:reload))
      (|toString| (|source| qml:*quick-view*)))))

(defun set-qml (url)
  (|setSource| qml:*quick-view* (qnew "QUrl(QString)" url))
  (|toString| (|source| qml:*quick-view*)))

(defun qml-reloaded ()
  ;; re-ini
  )

#+harbour-sextant
(qlater #'start)
