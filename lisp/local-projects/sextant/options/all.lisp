(uiop:define-package :sextant/options/all
  (:nicknames :options)
  (:use-reexport
   :sextant/options/config
   :sextant/options/options))
