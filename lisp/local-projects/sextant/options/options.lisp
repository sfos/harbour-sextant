(uiop:define-package :sextant/options/options
  (:use :cl :sextant/options/config)
  (:export #:push-action))
(in-package :sextant/options/options)

(set-config-package :sextant/options/options)

(defconfig agenda-files '("~/Documents/")
  :validate ((path) (not (null (probe-file path))))
  :get ((index) (nth index agenda-files))
  :set ((index path)
        (when (validate-agenda-files path)
          (let ((path (if (and (uiop:directory-pathname-p (probe-file path))
                             (not (uiop:directory-pathname-p (parse-namestring path))))
                          (concatenate 'string path "/")
                          path)))
            (loop while (<= (length agenda-files) index)
                  do (setf agenda-files (append agenda-files '(""))))
            (setf (nth index agenda-files) path)))))

(defconfig undo-history-size 100)
(defconfig recent-files-size 10)

(eval-when (:compile-toplevel :load-toplevel)
  (defun push-action (action-name types)
    (dolist (type types)
      (pushnew action-name (get (intern (concatenate 'string (symbol-name type) "-ACTIONS")
                                        #.*package*)
                                'actions)))))

(defmacro defaction-config (choices-list-symbol &rest config-name/value)
  `(progn
     ,(let ((list-getter (intern (concatenate 'string "GET-" (symbol-name choices-list-symbol)))))
        `(prog1
             (defun ,list-getter (index)
               (nth index (get ',choices-list-symbol 'actions)))
           (export ',list-getter)))
     ,@(loop for action on config-name/value by #'cddr
             collect `(defconfig ,(first action) ,(second action)
                        :set ((index)
                              (setf ,(first action) (nth index (get ',choices-list-symbol 'actions))))))))

(defaction-config org-line-actions
  click-on-org-line "RAW-EDIT"
  double-click-on-org-line "NOTHING"
  press-and-hold-on-org-line "NOTHING")

(defaction-config org-headline-actions
  click-on-org-headline "COLLAPSE/EXPAND"
  double-click-on-org-headline "NOTHING"
  press-and-hold-on-org-headline "RAW-EDIT")

(defconfig slynk-at-startup-p nil)
