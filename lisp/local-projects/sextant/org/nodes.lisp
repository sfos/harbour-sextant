(uiop:define-package :sextant/org/nodes
  (:use :cl :alexandria)
  (:export #:org-node
           #:org-document
           #:org-line
           #:org-headline
           #:previous-of
           #:next-of
           #:raw-text-of
           #:line-ending-of
           #:depth-of
           #:title-of
           #:previous-headline-of
           #:next-headline-of
           #:make-org-document
           #:make-org-line
           #:make-org-headline
           #:property-of
           #:headline-of
           #:visible-headline-of
           #:previous-visible-of
           #:previous-visible-headline-of
           #:next-visible-of
           #:next-visible-headline-of
           #:path-of
           #:node-visible-p
           #:count-nodes
           #:count-visible-nodes
           #:ensure-node-visible
           #:expand-node
           #:collapse-node
           #:compare-nodes
           #:swap-nodes
           #:append-node
           #:link-nodes))
(in-package :sextant/org/nodes)

(defclass org-node ()
  ((properties :initarg :properties
               :type list
               :initform nil
               :documentation "The property list of this node.")
   (previous :initarg :previous
             :type org-node
             :accessor previous-of
             :initform nil
             :documentation "The previous node.")
   (next :initarg :next
         :type org-node
         :accessor next-of
         :initform nil
         :documentation "The next node.")))

(defclass org-document (org-node)
  ())

(defclass org-line (org-node)
  ((raw-text :initarg :raw-text
             :type string
             :accessor raw-text-of
             :initform ""
             :documentation "The text in raw form contained in this node.")
   (line-ending :initarg :line-ending
                :type string
                :accessor line-ending-of
                :initform ""
                :documentation "The format of the line ending of this node.")))

(defclass org-headline (org-line)
  ((depth :initarg :depth
          :type number
          :accessor depth-of
          :initform 0
          :documentation "The depth of this headline.")
   (visibility :initarg :visibility
               :type keyword
               :accessor visibility-of
               :initform :expanded
               :documentation "The collapsed/expanded state of this headline.")
   (title :initarg :title
          :type string
          :accessor title-of
          :initform ""
          :documentation "The title of this headline.")
   (previous-headline :initarg :previous-headline
                      :type org-headline
                      :accessor previous-headline-of
                      :initform nil
                      :documentation "The headline preceding this one.")
   (next-headline :initarg :next-headline
                      :type org-headline
                      :accessor next-headline-of
                      :initform nil
                      :documentation "The headline following this one.")))

(defun make-org-document (first-node)
  (make-instance 'org-document :next first-node))

(defun make-org-line (raw-text line-ending)
  (make-instance 'org-line :raw-text raw-text :line-ending line-ending))

(defun make-org-headline (depth title raw-text line-ending)
  (make-instance 'org-headline :depth depth :visibility :expanded :title title
                               :raw-text raw-text :line-ending line-ending
                               :properties (list :visibility :expanded)))



(defgeneric property-of (node key &optional default))

(defmethod property-of ((node org-node) key &optional default)
  (with-slots (properties) node
    (getf properties key default)))

(defgeneric (setf property-of) (new-value node key &optional default))

(defmethod (setf property-of) (new-value (node org-node) key &optional default)
  (declare (ignore default))
  (with-slots (properties) node
    (setf (getf properties key) new-value)))



(defgeneric headline-of (node))

(defmethod headline-of ((node org-node))
  (loop for prev = (previous-of node) then (previous-of prev)
        while (and prev (not (subtypep (type-of prev) 'org-headline)))
        finally (return prev)))

(defmethod headline-of ((node org-headline))
  (loop for prev = (previous-headline-of node) then (previous-headline-of prev)
        while (and prev (>= (depth-of prev) (depth-of node)))
        finally (return prev)))

(defun visible-headline-of (node)
  (loop for h = (headline-of node) then (headline-of h)
        until (or (null h) (node-visible-p h))
        finally (progn
                  (assert h)
                  (return h))))



(defmethod depth-of ((node org-node))
  (let ((headline (headline-of node)))
    (if headline (1+ (depth-of headline)) 0)))

(defmethod previous-headline-of ((node org-node))
  (headline-of node))

(defmethod next-headline-of ((node org-node))
  (loop for next = (next-of node) then (next-of next)
        while (and next (not (subtypep (type-of next) 'org-headline)))
        finally (return next)))

(defmethod title-of ((node org-line))
  (raw-text-of node))

(defun collapsedp (node)
  (eq (visibility-of node) :collapsed))

(defun expandedp (node)
  (eq (visibility-of node) :expanded))



(defmethod previous-visible-of (node))

(defmethod previous-visible-of ((node org-node))
  (previous-of node))

(defmethod previous-visible-of ((node org-headline))
  (when-let ((prev (previous-of node)))
    (if (node-visible-p prev)
        prev
        (visible-headline-of prev))))

(defun previous-visible-headline-of (node)
  (when-let ((prev (previous-headline-of node)))
    (if (node-visible-p prev)
        prev
        (visible-headline-of prev))))

(defgeneric next-visible-of (node))

(defmethod next-visible-of ((node org-node))
  (next-of node))

(defmethod next-visible-of ((node org-headline))
  (if (expandedp node)
      (next-of node)
      (next-visible-headline-of node)))

(defun next-visible-headline-of (node)
  (loop for h = (next-headline-of node) then (next-headline-of h)
        until (or (null h) (node-visible-p h))
        finally (return h)))



(defun path-of (node &optional root)
  (let ((path (list node)))
    (loop for h = (headline-of node) then (headline-of h)
          until (or (null h) (eq h root))
          do (push h path)
          finally (progn
                    (assert (eq h root))
                    (return path)))))

(defun node-visible-p (node)
  (let ((headline (headline-of node)))
    (or (not headline) (expandedp headline))))

(defun count-nodes (begin end)
  (if (eq begin end)
      0
      (loop for n = (next-of begin) then (next-of n)
            count n into step
            until (or (null n) (eq n end))
            finally (return (and (eq n end) step)))))

(defun count-visible-nodes (begin end)
  (assert (node-visible-p begin))
  (assert (node-visible-p end))
  (if (eq begin end)
      0
      (loop for n = (next-visible-of begin) then (next-visible-of n)
            count n into step
            until (or (null n) (eq n end))
            finally (return (and (eq n end) step)))))



(defun ensure-node-visible (node)
  (unless (node-visible-p node)
    (let* ((root (visible-headline-of node))
           (headlines (cons root (path-of node root))))
      (mapc (lambda (h) (setf (visibility-of h) :expanded)) headlines))))



(defgeneric expand-node (node &optional recursep))

(defmethod expand-node ((node org-node) &optional recursep)
  (declare (ignore node recursep)))

(defmethod expand-node ((node org-headline) &optional recursep)
  (setf (visibility-of node) :expanded)
  (when recursep
    (let ((headlines (loop with depth = (depth-of node)
                           for h = (next-headline-of node) then (next-headline-of h)
                           until (or (null h) (<= (depth-of h) depth))
                           when (collapsedp h)
                             collect h)))
      (mapc (lambda (h) (setf (visibility-of h) :expanded)) headlines))))



(defgeneric collapse-node (node))

(defmethod collapse-node ((node org-node))
  (declare (ignore node)))

(defmethod collapse-node ((node org-headline))
  (let ((headlines (reverse (cons node (loop with depth = (depth-of node)
                                             for h = (next-visible-headline-of node) then (next-visible-headline-of h)
                                             until (or (null h) (<= (depth-of h) depth))
                                             when (expandedp h)
                                               collect h)))))
    (mapc (lambda (h) (setf (visibility-of h) :collapsed)) headlines)))



(defgeneric compare-nodes (old new))

(defmethod compare-nodes ((old org-line) (new org-line))
  :same)

(defmethod compare-nodes ((old org-headline) (new org-headline))
  (cond
    ((< (depth-of old) (depth-of new)) :headline-<)
    ((> (depth-of old) (depth-of new)) :headline->)
    (t :depth-=)))

(defmethod compare-nodes ((old org-headline) (new org-line))
  :depth->)

(defmethod compare-nodes ((old org-line) (new org-headline))
  :depth-<)



(defun %swap-nodes (old new)
  (when (previous-of old)
    (setf (previous-of new) (previous-of old)
          (next-of (previous-of old)) new))
  (when (next-of old)
    (setf (next-of new) (next-of old)
          (previous-of (next-of old)) new))
  new)

(defgeneric swap-nodes (old new))

(defmethod swap-nodes ((old org-line) (new org-line))
  (%swap-nodes old new))

(defmethod swap-nodes ((old org-headline) (new org-headline))
  (prog1
      (%swap-nodes old new)
    (when (previous-headline-of old)
      (setf (previous-headline-of new) (previous-headline-of old)
            (next-headline-of (previous-headline-of old)) new))
    (when (next-headline-of old)
      (setf (next-headline-of new) (next-headline-of old)
            (previous-headline-of (next-headline-of old)) new))
    (setf (slot-value new 'properties) (slot-value old 'properties))))

(defmethod swap-nodes ((old org-headline) (new org-line))
  (prog1
      (%swap-nodes old new)
    (when (previous-headline-of old)
      (setf (next-headline-of (previous-headline-of old)) (next-headline-of old)))
    (when (next-headline-of old)
      (setf (previous-headline-of (next-headline-of old)) (previous-headline-of old)))))

(defmethod swap-nodes ((old org-line) (new org-headline))
  (prog1
      (%swap-nodes old new)
    (let ((headline (headline-of old)))
      (if headline
          (progn
            (when (next-headline-of headline)
              (setf (next-headline-of new) (next-headline-of headline)
                    (previous-headline-of (next-headline-of headline)) new))
            (setf (next-headline-of headline) new
                  (previous-headline-of new) headline))
          (let ((headline (next-headline-of old)))
            (when headline
              (setf (previous-headline-of headline) new
                    (next-headline-of new) headline)))))))



(defun %append-node (node next)
  (assert (null (next-of node)))
  (assert (null (previous-of next)))
  (assert (null (next-of next)))
  (setf (next-of node) next
        (previous-of next) node)
  node)

(defgeneric append-node (node next))

(defmethod append-node ((node org-line) (next org-line))
 (%append-node node next))

(defmethod append-node ((node org-headline) (next org-headline))
  (prog1
      (%append-node node next)
    (setf (next-headline-of node) next
          (previous-headline-of next) node)))

(defmethod append-node ((node org-line) (next org-headline))
  (prog1
      (%append-node node next)
    (let ((prev-headline (headline-of node)))
      (when prev-headline
        (assert (null (next-headline-of prev-headline)))
        (setf (next-headline-of prev-headline) next
              (previous-headline-of next) prev-headline)))))

(defun link-nodes (&rest nodes)
  (loop for n on nodes
        while (second n)
        do (append-node (first n) (second n)))
  (first nodes))
