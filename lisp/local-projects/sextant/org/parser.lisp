(uiop:define-package :sextant/org/parser
  (:use :cl :alexandria
        :sextant/org/nodes
        :sextant/org/printer)
  (:export #:parse-document
           #:parse-node))
(in-package :sextant/org/parser)

(defun parse-document (spec)
  (let ((str (etypecase spec
               (string spec)
               ((or pathname stream) (read-file-into-string spec)))))
    (lexy::parse-document str)))

(defun parse-node (node)
  (lexy::parse-node (with-output-to-string (stream)
                      (org-print node stream))))
