(uiop:define-package :sextant/org/printer
  (:use :cl :sextant/org/nodes)
  (:export #:org-print))
(in-package :sextant/org/printer)

(defgeneric org-print (node stream &optional only-visible-p))

(defmethod org-print ((node org-document) stream &optional (only-visible-p nil))
  (loop for c = (next-of node) then (next-of c)
        while c
        do (org-print c stream only-visible-p)))

(defmethod org-print ((node org-line) stream &optional (only-visible-p nil))
  (when (or (node-visible-p node) (not only-visible-p))
    (princ (raw-text-of node) stream)
    (princ (line-ending-of node) stream)))
