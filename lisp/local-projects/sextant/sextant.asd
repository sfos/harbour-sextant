(defsystem sextant
  :serial t
  :defsystem-depends-on (:asdf-package-system)
  :class :package-inferred-system
  :around-compile (lambda (thunk)
                    (proclaim '(optimize (debug 3) (safety 3) (speed 0)))
                    (funcall thunk))
  :depends-on #.(append (uiop:read-file-form (merge-pathnames #p"dependencies.sexp" (or *load-pathname* *compile-file-pathname*)))
                        '("sextant/org/all")
                        '("sextant/options/all")
                        ;; '("sextant/models/all")
                        )
  :components ((:file "sextant")))

(register-system-packages "sextant/org/all" '(:org))
(register-system-packages "sextant/options/all" '(:options))
;; (register-system-packages "sextant/models/all" '(:models))
