TEMPLATE = subdirs
SUBDIRS = parser \
          bootstrap \
          sextant

parser.file = parser.pro
bootstrap.file = bootstrap.pro
bootstrap.depends = parser
sextant.file = sextant.pro
sextant.depends = parser bootstrap
