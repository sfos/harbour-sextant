#-eql5
(error "Please use the EQL5 executable")

(push :harbour-sextant *features*)

(asdf:make-build "sextant"
                 :monolithic t
                 :type :static-library
                 :move-here "./"
                 :init-name "init_sextant")

(let ((lib-name "libsextant.a"))
  (when (probe-file lib-name)
    (delete-file lib-name))
  (rename-file (x:cc "sextant--all-systems" ".a") lib-name))
