QT += widgets
TEMPLATE = app
TARGET = sextant-bootstrap
DESTDIR = $$PWD
OBJECTS_DIR = $$PWD/tmp/bootstrap
LIBS += -lecl -leql5 -lsextant-parser -L.
QMAKE_CXXFLAGS += -std=c++2a -Wno-parentheses -Wno-unused-local-typedefs -Wno-array-bounds -Wno-maybe-uninitialized -Wno-restrict
QMAKE_LFLAGS += "-Wl,-rpath,\'\$$ORIGIN\'"

SOURCES += src/bootstrap.cc
