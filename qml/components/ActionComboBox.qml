import QtQuick 2.0
import Sailfish.Silica 1.0
import EQL5 1.0

ComboBox {
    id: actionComboBox
    width: parent.width

    property string list
    property string getter
    property string setter

    menu: ContextMenu {
        Repeater {
            model: ListModel {

                function populate () {
                    var current = Lisp.call(getter)
                    var i = 0
                    var item = Lisp.call(list, i)
                    while (item) {
                        append({inputText: item})

                        if (item == current)
                            actionComboBox.currentIndex = i

                        item = Lisp.call(list, ++i)
                    }
                }

                Component.onCompleted: populate()
            }

            MenuItem {
                text: inputText
            }
        }
    }

    onCurrentItemChanged: {
        Lisp.call(setter, currentIndex)
    }
}
