import QtQuick 2.0
import Sailfish.Silica 1.0

MouseArea {
    id: orgText

    property int contentHeight
    property bool editing: false

    height: editing ? edit.height : contentHeight

    anchors {
        left: parent.left
        right: parent.right
    }

    OrgEdit {
        id: edit
        visible: editing

        onLoseFocus: {
            editing = false
        }
    }

    function setFocus() { document.focusedIndex = index }
    function editRawText() { editing = true }

    function forceCommit (update) { edit.forceCommit(update) }
    function setCursorPosition(index) { edit.setCursorPosition(index) }
    function setCursorPositionAt (x, y) { edit.setCursorPositionAt(x, y) }
    function setCursorPositionAtEnd (fix) { edit.setCursorPositionAtEnd(fix) }
}
