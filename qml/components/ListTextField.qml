import QtQuick 2.0
import Sailfish.Silica 1.0
import EQL5 1.0

Column {
    id: listTextField
    width: parent.width

    property string label
    property string getter
    property string setter

    Repeater {
        id: repeater

        property bool forceActiveFocus: false

        model: ListModel {
            id: listModel

            function populate () {
                var i = 0
                var item = Lisp.call(getter, i)
                while (item) {
                    append({inputText: item})
                    item = Lisp.call(getter, ++i)
                }

                if (count == 0)
                    append({inputText: ""})
            }

            Component.onCompleted: populate()
        }

        Item {
            id: item
            width: listTextField.width
            height: field.height

            TextField {
                id: field
                width: parent.width - icon.width

                label: index == repeater.count - 1 ? listTextField.label : ""
                placeholderText: listTextField.label
                text: inputText

                onTextChanged: {
                    acceptableInput = Lisp.call(setter, index, text)
                    if (acceptableInput)
                        inputText = text
                }

                Component.onCompleted: {
                    if (repeater.forceActiveFocus)
                    {
                        forceActiveFocus()
                        repeater.forceActiveFocus = false
                    }
                }
            }

            IconButton {
                id: icon
                visible: field.text != ""
                anchors.left: field.right
                icon.source: "image://theme/icon-m-" + (index == repeater.count - 1 ? "add" : "remove")
                onClicked: {
                    if (index == repeater.count - 1) {
                        repeater.forceActiveFocus = true
                        listModel.append({inputText: ""})
                    }
                    else
                        listModel.remove(index)
                }
            }
        }

    }
}
