import QtQuick 2.0
import Sailfish.Silica 1.0
import EQL5 1.0

TextArea {
    id: orgEdit

    property string sentinelChar:  "⁣" // there is an invisible char here
    property string lastText: index >= 0 ? rawtext : ""
    property bool textModified: false
    property int fixedCursorPosition: -1

    signal loseFocus

    text: index > 0 ? sentinelChar + rawtext : (index == 0 ? rawtext : "")

    anchors {
        top: parent.top
        left: parent.left
        right: parent.right
    }

    textMargin: 0
    textTopMargin: 0
    wrapMode: Text.Wrap
    labelVisible: false
    inputMethodHints: Qt.ImhNoAutoUppercase

    onCursorPositionChanged: { refreshCursorPosition() }

    onSelectionStartChanged: {
        if (index > 0 && selectionStart == 0)
            select(1, selectionEnd)
    }

    onTextChanged: {
        if (visible) {
            refreshCursorPosition()
            if (index >= 0) {
                if (index > 0 && text[0] != sentinelChar) {
                    var textEmpty = text.length == 0
                    forceCommit(false)
                    /* document.focusedIndex = index - 1 */
                    /* var focusedItem = document.focusedItem */
                    /* focusedItem.setCursorPositionAtEnd(!textEmpty) */
                    Lisp.call("models:join-node", index, true)
                    /* focusedItem.editRawText() */
                } else {
                    var split = text.indexOf("\n")
                    if (split != -1) {
                        forceCommit(false)
                        Lisp.call("models:split-node", index, text.substring(index > 0 ? 1 : 0, split), text.substring(split + 1), true)
                        /* document.focusedIndex = index + 1 */
                        /* document.focusedItem.editRawText() */
                    } else {
                        lastText = getText()
                        textModified = lastText != rawtext
                        undo.enabled = textModified || Lisp.call("models:can-undo-p")
                        redo.enabled = !textModified && Lisp.call("models:can-redo-p")
                        save.enabled = textModified || Lisp.call("models:can-save-p")
                    }
                }
            }
        }
    }

    onActiveFocusChanged: {
        if (!activeFocus) {
            if (document.focusedIndex == index)
                document.focusedIndex = -1

            forceCommit(true)
            loseFocus()
        }
    }

    onVisibleChanged: initFocus()
    Component.onCompleted: initFocus()

    function getText () {
        return index > 0 ? text.substring(1) : text
    }

    function initFocus() {
        if (visible) {
            forceActiveFocus()
            refreshCursorPosition()
        }
    }

    function refreshCursorPosition () {
        if (fixedCursorPosition != -1) {
            cursorPosition = fixedCursorPosition
            fixedCursorPosition = -1
        }

        if (index > 0 && cursorPosition == 0)
            cursorPosition = 1
    }

    function forceCommit (update) {
        if (textModified) {
            textModified = false
            Lisp.call("models:modify-text", index, lastText, update)
        }
    }

    function setCursorPosition(index) {
        cursorPosition = index
    }

    function setCursorPositionAt(x, y) {
        cursorPosition = positionAt(x, y)
    }

    function setCursorPositionAtEnd (fix) {
        if (fix) {
            console.log("fixed " + text.length)
            fixedCursorPosition = text.length
        } else {
            cursorPosition = text.length
        }
    }
}
