import QtQuick 2.0
import Sailfish.Silica 1.0
import EQL5 1.0

OrgText {
    id: orgHeadline

    contentHeight: titleLabel.contentHeight

    Label {
        id: bullet
        anchors {
            top: parent.top
            left: parent.left
        }

        font {
            pixelSize: Theme.fontSizeExtraLarge
            bold: true
            family: Theme.fontFamilyHeading
        }

        visible: !editing
        text: "*"
    }

    Label {
        id: titleLabel
        anchors {
            top: parent.top
            left: bullet.right
            right: parent.right
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingSmall
        }

        font {
            bold: true
            family: Theme.fontFamilyHeading
        }

        visible: !editing
        text: title
        wrapMode: Text.Wrap
    }

    onClicked: {
        Lisp.call(this, "models:org-headline-clicked", index, mouse.x, mouse.y)
    }

    onDoubleClicked: {
        Lisp.call(this, "models:org-headline-double-clicked", index, mouse.x, mouse.y)
    }

    onPressAndHold: {
        Lisp.call(this, "models:org-headline-press-and-hold", index, mouse.x, mouse.y)
    }
}
