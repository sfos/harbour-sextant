import QtQuick 2.0
import Sailfish.Silica 1.0
import EQL5 1.0

OrgText {
    id: orgLine

    contentHeight: label.contentHeight

    Label {
        id: label
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            rightMargin: Theme.paddingSmall
        }

        visible: !editing
        text: rawtext
        wrapMode: Text.Wrap
    }

    onClicked: {
        Lisp.call(this, "models:org-line-clicked", index, mouse.x, mouse.y)
    }

    onDoubleClicked: {
        Lisp.call(this, "models:org-line-double-clicked", index, mouse.x, mouse.y)
    }

    onPressAndHold: {
        Lisp.call(this, "models:org-line-press-and-hold", index, mouse.x, mouse.y)
    }

}
