import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    id: orgItem

    property bool focused: document.focusedIndex == index

    anchors {
        left: parent.left
        right: parent.right
        leftMargin: Theme.paddingMedium * depth
    }

    height: loader.item ? loader.item.height : 0

    Loader {
        id: loader
        anchors.fill: parent

        property string type: nodeType

        onTypeChanged: {
            var url
            if (type == "org-line")
                url = "OrgLine.qml"
            else if (type == "org-headline")
                url = "OrgHeadline.qml"

            setSource(url)
        }
    }

    GlassItem {
        id: eof
        visible: isLastItem
        anchors.top: orgItem.bottom
        height: Theme.paddingLarge
        width: parent.width
        falloffRadius: 0.15
        radius: 0.15
        color: palette.highlightColor
        cache: true
    }

    onFocusedChanged: {
        if (focused)
            document.focusedItem = orgItem
    }

    function forceCommit (update) { loader.item.forceCommit(update) }
    function setCursorPosition(index) { loader.item.setCursorPosition(index) }
    function setCursorPositionAtEnd (fix) { loader.item.setCursorPositionAtEnd(fix) }
    function editRawText () { loader.item.editRawText() }
}
