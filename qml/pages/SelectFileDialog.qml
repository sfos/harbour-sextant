import QtQuick 2.0
import Sailfish.Silica 1.0
import EQL5 1.0

Dialog {
    id: dialog
    property string text
    property string filename: ""
    property string directory: ""
    property bool pos0: false

    Column {
        width: parent.width

        DialogHeader { }

        Label {
            id: label
            width: parent.width
            text: dialog.text
            wrapMode: Text.Wrap
        }

        TextField {
            id: fileNameEdit
            width: parent.width
            label: qsTr("Filename")
            text: filename

            Component.onCompleted: {
                if (pos0) {
                    cursorPosition = 0
                }
                forceActiveFocus()
            }
        }

        ComboBox {
            id: directories
            width: parent.width
            label: qsTr("Directory")

            menu: ContextMenu {
                Repeater {
                    id: repeater

                    model: ListModel {
                        id: listModel

                        function populate () {
                            if (directory != "") {
                                append({content: directory})
                            }

                            var i = 0
                            var item = Lisp.call("options:get-agenda-files", i)
                            while (item) {
                                if (item != directory) {
                                    append({content: item})
                                }
                                item = Lisp.call("options:get-agenda-files", ++i)
                            }

                            if (count == 0)
                                append({content: ""})
                        }

                        Component.onCompleted: populate()
                    }

                    MenuItem {
                        text: content
                    }
                }
            }
        }

        Label {
            id: agendaFilesHist
            width: parent.width
            text: qsTr("Customize directories list in Settings page.")
            wrapMode: Text.Wrap
        }
    }

    onAccepted: {
        filename = fileNameEdit.text
        directory = directories.value
    }
}
