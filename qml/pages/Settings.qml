import QtQuick 2.0
import Sailfish.Silica 1.0
import EQL5 1.0
import "../components/"

Page {
    id: settings
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Settings")
            }

            ListTextField {
                label: "Agenda files or directories"
                getter: "options:get-agenda-files"
                setter: "options:set-agenda-files"
            }

            Slider {
                label: "Undo history size"
                width: parent.width
                minimumValue: 100
                maximumValue: 1024
                stepSize: 1
                value: Lisp.call("options:get-undo-history-size")
                valueText: value
                onValueChanged: Lisp.call("options:set-undo-history-size", value)
            }

            Slider {
                label: "Recent files size"
                width: parent.width
                minimumValue: 0
                maximumValue: 100
                stepSize: 1
                value: Lisp.call("options:get-recent-files-size")
                valueText: value
                onValueChanged: Lisp.call("options:set-recent-files-size", value)
            }

            ExpandingSectionGroup {
                currentIndex: -1

                ExpandingSection {
                    id: actions
                    title: qsTr("Actions")

                    content.sourceComponent: Column {
                        width: actions.width

                        SectionHeader {
                            text: qsTr("Line")
                        }

                        ActionComboBox {
                            label: qsTr("On clicked")
                            list: "options:get-org-line-actions"
                            getter: "options:get-click-on-org-line"
                            setter: "options:set-click-on-org-line"
                        }

                        ActionComboBox {
                            label: qsTr("On double-clicked")
                            list: "options:get-org-line-actions"
                            getter: "options:get-double-click-on-org-line"
                            setter: "options:set-double-click-on-org-line"
                        }

                        ActionComboBox {
                            label: qsTr("On press and hold")
                            list: "options:get-org-line-actions"
                            getter: "options:get-press-and-hold-on-org-line"
                            setter: "options:set-press-and-hold-on-org-line"
                        }

                        SectionHeader {
                            text: qsTr("Headline")
                        }

                        ActionComboBox {
                            label: qsTr("On clicked")
                            list: "options:get-org-headline-actions"
                            getter: "options:get-click-on-org-headline"
                            setter: "options:set-click-on-org-headline"
                        }

                        ActionComboBox {
                            label: qsTr("On double-clicked")
                            list: "options:get-org-headline-actions"
                            getter: "options:get-double-click-on-org-headline"
                            setter: "options:set-double-click-on-org-headline"
                        }

                        ActionComboBox {
                            label: qsTr("On press and hold")
                            list: "options:get-org-headline-actions"
                            getter: "options:get-press-and-hold-on-org-headline"
                            setter: "options:set-press-and-hold-on-org-headline"
                        }
                    }
                }

                ExpandingSection {
                    id: development
                    title: qsTr("Development")

                    content.sourceComponent: Column {
                        width: section.width

                        TextSwitch {
                            text: qsTr("Enable Slynk at startup")
                            checked: Lisp.call("options:get-slynk-at-startup-p")
                            onCheckedChanged: Lisp.call("options:set-slynk-at-startup-p", checked)
                        }

                        Button {
                            text: qsTr("Start")
                            visible: false
                            enabled: !Lisp.call("sextant:slynkp")
                            onClicked: {
                                Lisp.call("sextant:start-slynk")
                                enabled = !Lisp.call("sextant:slynkp")
                            }
                        }
                    }
                }
            }
        }
    }
}
