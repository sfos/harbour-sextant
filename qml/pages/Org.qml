import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components/"
import EQL5 1.0

Page {
    id: document
    objectName: "orgDocument"
    allowedOrientations: Orientation.All

    property int focusedIndex: -1
    property var focusedItem
    property int nextCursorPosition: -1
    property string filepath
    property string filename

    SilicaListView {
        id: listView
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: toolbar.top
        }

        clip: true

        header: PageHeader {
            title: filename
        }

        model: orgModel
        delegate: OrgDelegate {}
    }

    onFocusedIndexChanged: {
        listView.currentIndex = focusedIndex
        if (focusedIndex == -1)
            focusedItem = null
    }

    function focusIndex(index, edit, cursorPos) {
        focusedIndex = index
        if (edit) {
            focusedItem.setCursorPosition(cursorPos)
            focusedItem.editRawText()
        }
    }

    function refreshToolbar(undoEnabled, redoEnabled, saveEnabled) {
        undo.enabled = undoEnabled
        redo.enabled = redoEnabled
        save.enabled = saveEnabled
    }

    DockedPanel {
        id: toolbar

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        height: Theme.itemSizeMedium
        dock: Dock.Bottom
        open: true

        IconButton {
            id: undo
            width: height
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            icon.source: "image://theme/icon-m-rotate-left"
            enabled: false

            onClicked: {
                if (document.focusedItem != null) {
                    document.focusedItem.forceCommit(true);
                    document.focusedIndex = -1
                }

                var index = Lisp.call("models:undo")
                listView.currentIndex = index
            }
        }

        IconButton {
            id: redo
            width: height
            anchors.left: undo.right
            anchors.verticalCenter: parent.verticalCenter
            icon.source: "image://theme/icon-m-rotate-right"
            enabled: false

            onClicked: {
                document.focusedIndex = -1
                var index = Lisp.call("models:redo")
                listView.currentIndex = index
            }
        }

        IconButton {
            id: save
            width: height
            anchors.right: settings.left
            anchors.verticalCenter: parent.verticalCenter
            icon.source: "image://theme/icon-m-sd-card"
            enabled: false

            onClicked: {
                if (document.focusedItem != null) {
                    document.focusedItem.forceCommit(true)
                    document.focusedIndex = -1
                }

                if (!Lisp.call("models:save-file", filepath)) {
                    var dir = filepath.substring(0, filepath.lastIndexOf('/') + 1)
                    var dialog = pageStack.push(Qt.resolvedUrl("SelectFileDialog.qml"),
                                                {text: qsTr("File was modified by another program. Overwrite or choose another filename."),
                                                 filename: filename, directory: dir})
                    dialog.accepted.connect(function() {
                        filepath = dialog.directory + dialog.filename
                        Lisp.call("models:save-file", filepath, true)
                        filename = dialog.filename
                    })
                }
            }
        }

        IconButton {
            id: settings
            width: height
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            icon.source: "image://theme/icon-m-setting"
        }
    }

    onStatusChanged: {
        if (status == PageStatus.Inactive) {
            Lisp.call("sextant:close-file", filepath)
        }
    }
}
