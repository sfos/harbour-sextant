import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import EQL5 1.0

Page {
    id: files
    allowedOrientations: Orientation.All

    SilicaListView {
        id: listView
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Files")
        }

        model: agendaFilesModel

        section {
            property: 'section'

            delegate: SectionHeader {
                text: section == "recentf" ? qsTr("Recent files") : section
                height: Theme.itemSizeExtraSmall
            }
        }

        delegate: ListItem {
            id: fileItem

            anchors {
                left: parent.left
                right: parent.right
            }
            contentHeight: Theme.itemSizeSmall

            Label {
                text: section == "recentf" ? directory + filename : filename
                elide: Text.ElideMiddle
                width: parent.width - Theme.horizontalPageMargin * 2
                font.pixelSize: Theme.fontSizeMedium
                anchors {
                    top: parent.top
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    centerIn: parent
                }
            }

            onClicked: {
                var filepath = directory + filename
                Lisp.call("sextant:open-file", filepath)
                pageStack.push(Qt.resolvedUrl("Org.qml"), {filepath: filepath, filename: filename})
            }

            RemorseItem { id: remorse }

            menu: ContextMenu {
                MenuItem {
                    visible: section == "recentf"
                    text: qsTr("Remove from list")
                    onClicked: {
                        remorse.execute(fileItem, "", function () {
                            var filepath = directory + filename
                            Lisp.call("models:remove-from-files-list", filepath)
                        })
                    }
                }

                MenuItem {
                    text: qsTr("Delete file")
                    onClicked: {
                        remorse.execute(fileItem, "", function () {
                            var filepath = directory + filename
                            Lisp.call("sextant:delete-file*", filepath)
                        })
                    }
                }
            }
        }

        ViewPlaceholder {
            id: placeholder
            enabled: listView.count == 0
            text: qsTr("Pull down to open a file")
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("Settings.qml"))
            }

            MenuItem {
                text: qsTr("Open file")
                onClicked: pageStack.push(filePickerPage)
            }

            MenuItem {
                text: qsTr("New file")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("SelectFileDialog.qml"),
                                                {text: qsTr("Create new file"),
                                                 filename: ".org", pos0: true,
                                                 acceptDestination: Qt.resolvedUrl("Org.qml"),
                                                 acceptDestinationAction: PageStackAction.Replace})
                    dialog.accepted.connect(function() {
                        var filepath = dialog.directory + dialog.filename
                        Lisp.call("sextant:open-file", filepath)
                        dialog.acceptDestinationInstance.filepath = filepath
                        dialog.acceptDestinationInstance.filename = dialog.filename
                    })
                }
            }
        }

        PushUpMenu {
            MenuItem {
                text: qsTr("Scroll to top")
                onClicked: scrollToTop()
            }
        }

        Component {
            id: filePickerPage
            FilePickerPage {
                popOnSelection: false
                onSelectedContentPropertiesChanged: {
                    if (selectedContentProperties.filePath) {
                        Lisp.call("sextant:open-file", selectedContentProperties.filePath)
                        pageStack.replaceAbove(files, Qt.resolvedUrl("Org.qml"),
                                               {filepath: selectedContentProperties.filePath,
                                                filename: selectedContentProperties.fileName})
                    }
                }
            }
        }
    }

    onStatusChanged: {
        if (status == PageStatus.Activating) {
            Lisp.call("sextant:refresh-agenda-files")
        }
    }
}
